# :green_heart: actions-template

[![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green?style=flat-square)](https://GitHub.com/GregoireF/actions-template/graphs/commit-activity)
[![Ask Me Anything !](https://img.shields.io/badge/Ask%20me-anything-1abc9c?style=flat-square)](https://github.com/GregoireF/actions-template/issues)
[![GitHub license](https://img.shields.io/github/license/GregoireF/actions-template?style=flat-square)](https://github.com/GregoireF/actions-template/blob/master/LICENSE)
[![GitHub release](https://img.shields.io/github/v/release/GregoireF/actions-template?include_prereleases&style=flat-square)](https://github.com/GregoireF/actions-template/releases/)
[![Github all releases](https://img.shields.io/github/downloads/GregoireF/actions-template/total?style=flat-square)](https://github.com/GregoireF/actions-template/releases/)
[![GitHub forks](https://img.shields.io/github/forks/GregoireF/actions-template?style=flat-square)](https://github.com/GregoireF/actions-template/network/)
[![GitHub stars](https://img.shields.io/github/stars/GregoireF/actions-template?style=flat-square)](https://github.com/GregoireF/actions-template/stargazers/)
[![GitHub contributors](https://img.shields.io/github/contributors/GregoireF/actions-template?style=flat-square)](https://github.com/GregoireF/actions-template/graphs/contributors)

## actions-template is a project to include github-actions for each start of a new project via Github.com. This project is intended to help create consistency in open source collaborative development.

### What can I offer you?

- :books: PR Template: Added a default template for PRs
- :books: PR Emoji List: Added a default template for Issues
- :books: Bug Report Template: Added a default template for PRs
- :construction_worker: TypeScript Compiler: TypeScript code checking
- :construction_worker: Triage: Putting the issues away
- :construction_worker: Pull Request Labeler: Apply useful labels according to the changes
- :construction_worker: Pull Request: Automatic assignment of the PR to the creator of the PR
- :construction_worker: Lock Threads: Closing of threads in certain cases (age of thread, inactivity,...)
- :construction_worker: Linter Code: Code cleaning according to Lint conventions
- :construction_worker: Compress Image: Compression of images at each addition in order to reduce their weight (PNG,JPEG,WEBP,GIF,...)
- :green_heart: CI: Yarn Environment: Installing a NodeJS environment with Yarn
  > _More coming soon_

##

### Get Started

First of all, consider forking the project at home so that you can use it.

##

### Become a contributor

Developers interested in contributing should read the [Code of Conduct](./CODE_OF_CONDUCT.md) and the [Contribution Guide](https://google.com).

> Please do **not** ask general questions in an issue. Issues are only to report bugs, suggest
> enhancements, or request new features.

##

### This project was initiated when new projects were created via Github to ensure code consistency throughout development. Many thanks to [the Github community](https://github.com/marketplace) for sharing various Github actions.
